using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Referencias 
    public BallController   ball;
    public PaddleController paddle;
    public GameObject laser;
    public bool laserEnabled = false;
    public float currentTime = 0f;
    public float startingTime = 3;
    public HUDController hud;
    public SceneController sceneController;
    // Popup cuando el juego termina
    public GameObject gameOverHUD;
    public Text finalScore;
    public Text gameWinText;
    public Text gameOverText;

    // PAR�METROS
    // Total de vidas
    public int lives = 3;
    // Vidas actuales
    public int currentLives;
    // Puntuaci�n 
    public int score = 0;
    public int scoreToUpdate = 10;
    // Niveles
    public GameObject[] levels;
    // Padre del nivel
    public Transform levelParent;
    // �ndice del nivel actual
    public int currentLevelIndex;
    // Referencia al nivel actual
    public GameObject currentLevel;
    // Bloques restantes para completar el nivel
    public int pendingBlocks;
    // COmprueba si el juego acaba
    public bool gameOver = false;

    // Instance del Singleton
    public static GameManager instance;

    private void Awake() {
        // Inicializamos la instancia del Singleton
        if(instance == null) {
            instance = this;
        } else {
            // Si ya est� inicializado, destruimos ESTE objeto
            Destroy(this);
        }
    }

    void Start(){
        // Cargamos el nivel inicial 
        gameOverHUD.SetActive(false);
        NextLevel();
        currentLives = lives;
        hud.UpdateLivesText(currentLives.ToString());
        currentTime = startingTime;
    }

    void Update(){

        if (gameOver) {
            Restart();
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            sceneController.loadScene("Menu");
            // Reseteamos la canci�n para que vuelva a sonar en este caso
            if (gameOver) {
                AudioManager.instance.PlaySound("MainTheme");
            }
        }

        if (laserEnabled) {
            Timer();
        }
    }

    /// <summary>
    /// Gestiona la carga del siguiente nivel
    /// </summary>
    private void NextLevel() {
        // Si a�n quedan niveles por cargar
        if(levels.Length > currentLevelIndex) {
            // Si existe un nivel cargado
            if(currentLevel != null) {
                // Lo destruimos
                Destroy(currentLevel);
            }
            // Obtenemos el pr�ximo nivel
            // OJO: PRIMERO ACCEDE AL ACTUAL Y LUEGO INCREMENTA. NO ACCEDE AL SIGUIENTE
            GameObject levelPrefab = levels[currentLevelIndex++];
            // Equivalecia:
            // GameObject levelPrefab = levels[currentLevelIndex];
            // currentLevelIndex ++

            // Instanciamos el nivel como copiar el objeto
            currentLevel = Instantiate(levelPrefab, levelParent);
            // Obtenemos el total de bloques
            pendingBlocks = currentLevel.GetComponentsInChildren<BlockController>().Length;

        } else {
            // Juego completado
            gameOverHUD.SetActive(true);
            gameOverText.gameObject.SetActive(false);
            AudioManager.instance.StopSound("MainTheme");
            AudioManager.instance.PlaySound("GameWin");
            finalScore.text = score.ToString();
        }
    }

    /// <summary>
    /// Reseta el juego por completo hasta el principio.
    /// </summary>
    private void Restart() {
        if (Input.GetKeyDown(KeyCode.R)) {
            currentLevelIndex = 0;
            gameOverHUD.SetActive(false);
            paddle.canMove = true;
            ball.canStartMovement = true;
            currentLives = 3;
            score = 0;
            hud.UpdateScoreText(score.ToString());
            hud.UpdateLivesText(currentLives.ToString());
            NextLevel();
            AudioManager.instance.PlaySound("MainTheme");
        }
    }

    /// <summary>
    /// Realiza las acciones tras destruir un bloque
    /// </summary>
    public void BlockHasBeenRemoved() {
        pendingBlocks--;
        if(pendingBlocks <= 0) {
            Debug.Log("Nivel Completado");
            // Resetamos la bola
            ball.isMoving = false;
            AudioManager.instance.PlaySound("Success");
            // Cargamos el siguiente nivel
            NextLevel();
        }
    }

    /// <summary>
    /// Gestiona cuando el jugador pierde una vida
    /// </summary>
    public void LifeLost() {
        currentLives--;
        AudioManager.instance.PlaySound("Death");
        hud.UpdateLivesText(currentLives.ToString());
        if(currentLives <= 0) {
            // Mostramos el menu de gameover
            gameOverHUD.SetActive(true);
            gameWinText.gameObject.SetActive(false);
            AudioManager.instance.StopSound("MainTheme");
            AudioManager.instance.PlaySound("GameOver");
            finalScore.text = score.ToString();
            paddle.canMove = false;
            ball.canStartMovement = false;
            gameOver = true;
        }
    }

    /// <summary>
    /// Cambia el valor de la etiqueta de Score en el HUD en funci�n de la 
    /// puntuaci�n que tengamos para a�adir (scoreToUpdate) puede verse 
    /// afectado por el PU de double score.
    /// </summary>
    public void UpdateScore() {
        score += scoreToUpdate;
        hud.UpdateScoreText(score.ToString());
    }

    /// <summary>
    /// A�ade una vida al jugador (llamado desde el PU del coraz�n)
    /// </summary>
    public void AddLifePU() {
        currentLives++;
    }

    /// <summary>
    /// Activa los l�seres del paddle
    /// </summary>
    public void EnableLaserPowerUp() {
        laser.SetActive(true);
        laserEnabled = true;
    }

    /// <summary>
    /// Temporizador para la duraci�n del laser
    /// </summary>
    private void Timer() {
        currentTime -= 1 * Time.deltaTime;
        if (currentTime <= 0) {
            laser.SetActive(false);
            currentTime = startingTime;
            laserEnabled = false;
        }
    }
}
