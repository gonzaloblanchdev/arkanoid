using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    // Proyectil que se lanza (Objeto que vamos a instanciar)
    public GameObject proyectile;
    // Padre para instanciar el objeto.
    public Transform proyectileParent;
    
    void Update()
    {
        // Instanciamos el proyectil cada vez que se presione el espacio
        if (Input.GetButtonDown("Jump")){
            Instantiate(proyectile, transform.position, Quaternion.identity);
            AudioManager.instance.PlaySound("LaserShot");
        }
    }

}
