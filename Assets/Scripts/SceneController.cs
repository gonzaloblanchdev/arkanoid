using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    /// <summary>
    /// Carga la escena "sceneName"
    /// </summary>
    /// <param name="sceneName"></param>
    public void loadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// Termina la ejecución del juego
    /// </summary>
    public void QuitGame() {
        // Para que funcione en el editor, comprobamos si estamos en el
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
}