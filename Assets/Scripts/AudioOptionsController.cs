using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Como estoy usando Audio Manager con singletone, si muteo desde el audiomanager
// no funciona bien, por lo que he hecho esta clase a modo de interfaz para 
// asegurarme que siempre funcione.

// Estas funciones son exclusivas para botones de tipo toggle

public class AudioOptionsController : MonoBehaviour
{
    /// <summary>
    /// Llama al audiomanager para mutear la m�sica.
    /// </summary>
    /// <param name="newState"></param>
    public void MuteMusic(bool newState) {
        AudioManager.instance.MuteMusic(newState);
    }
    /// <summary>
    /// Llama al audiomanager para mutear los efectos especiales.
    /// </summary>
    /// <param name="newState"></param>
    public void MuteFX(bool newState) {
        AudioManager.instance.MuteFX(newState);
    }
}
