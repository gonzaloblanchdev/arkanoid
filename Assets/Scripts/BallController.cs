using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [Header("Movement")]
    // Indica si la bola esta en movimiento
    public bool isMoving = false;
    // Indica si la bola puede comenzar a moverse
    public bool canStartMovement = true;
    // Velocidad de movimiento de la bola
    public float moveSpeed = 10;
    // Elemento al que la bola seguir� si esta no est� en movimiento
    public Transform stoppedTarget;
    // Margen que se aplica a la posici�n del stoppedTarget cuando la bola 
    // no est� en movimiento
    public Vector2 stoppedOffset;

    [Header("Collisions")]
    // Longitud de los rayos para detectar colisiones
    public float rayLenght = 0.1f;
    // Margen para el punto de origen
    public float rayOffset = 0.1f;

    // Almacena la velocidad actual de la bola durante su movimiento
    private Vector2 _velocity; // Gui�n bajo para indicar que es privada con el nombre

    // Se ejecuta en tiempo de edici�n cada vez que hay una modificaci�n
    // en este componente
    private void OnValidate() {
        if (stoppedTarget == null) {
            return;
        }
        transform.position = stoppedTarget.position + (Vector3) stoppedOffset;
    }

    void Update()
    {
        if (!isMoving) {
            Follow();
            if (Input.GetButtonDown("Jump") && canStartMovement) {
                StartMovement();
            }
        }

        // Lanzamos los rayos en todas las direcciones
        bool right = _velocity.x >= 0 && CheckCollision(transform.right);
        bool left  = _velocity.x <= 0 && CheckCollision(-transform.right);
        bool up    = _velocity.y >= 0 && CheckCollision(transform.up);
        bool down  = _velocity.y <= 0 && CheckCollision(-transform.up);

        // Si hay colisi�n por los lados
        if(left || right) {
            // Invertimos la velocidad en el eje X
            _velocity.x *= -1;
        }
        // Si hay colisi�n arriba o abajo
        if(up || down || (left && right)) {
            // Invertimos la velocidad en el eje Y
            _velocity.y *= -1;
        }

        // Aplicamos el movimiento
        transform.position += (Vector3) _velocity * Time.deltaTime;
    }

    // Permite dibujar gizmos en la escena del juego para tener referencia visual
    // de ciertos elementos. Como por ejemplo, raycasts.
    private void OnDrawGizmos() {
        // Indicamos el color del gizmo
        Gizmos.color = Color.red;

        // Los dibujamos
        DrawRayGizmo(transform.up);
        DrawRayGizmo(transform.right);
        DrawRayGizmo(-transform.right);
        DrawRayGizmo(-transform.up);
    }

    /// <summary>
    /// Dibuja un gizmo en la direcci�n indicada por �
    /// </summary>
    /// <param name="direction"></param>
    private void DrawRayGizmo(Vector3 direction) {
        Vector3 from = transform.position + direction * rayOffset;
        Vector3 to = from + direction * rayLenght;
        Gizmos.DrawLine(from, to);
    }

    /// <summary>
    /// Durante el estado de no movimiento este m�todo se encargar�
    /// de mover la bola a la posici�n de su stoppedTarget.
    /// </summary>
    private void Follow() {
        // Si hay movimiento
        if (_velocity.magnitude > 0) {
            // Reseteamos el vector a cero
            _velocity = Vector2.zero;
        }

        // Asignamos la posici�n del stoppedTarget
        transform.position = stoppedTarget.position + (Vector3) stoppedOffset;
    }

    /// <summary>
    /// Inicia el movimiento de la bola
    /// </summary>
    private void StartMovement() {
        // Cambiamos el estado de la bola
        isMoving = true;
        // Calculamos la direcci�n en la que aplicamos el movimiento
        Vector3 direction = transform.position - stoppedTarget.position;
        // Calculamos la velocidad de movimiento en la direcci�n anterior
        _velocity = direction.normalized * moveSpeed;
    }
    
    /// <summary>
    /// Comprueba si hay alg�n elemento conel 
    /// que colisionar en la direcci�n indicada
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    private bool CheckCollision(Vector2 direction) {
        // Lanzamos un raycast indicando el origen direcci�n y distancia
        // Guardamos el resultado en la variable hit.
        Vector3 from = transform.position + (Vector3) direction * rayOffset;
        RaycastHit2D hit = Physics2D.Raycast(from, direction, rayLenght);
        // Si el transform no es nulo quiere decir que hay colisi�n
        if (hit.transform == null || hit.transform == transform || hit.transform.CompareTag("PowerUp")) {
            return false;
        }

        // Si ha colisionado con el jugador
        if (hit.transform.CompareTag("Player") && hit.transform.TryGetComponent(out PaddleController paddle)) {
            // Ejecutamos el m�todo de rebote con este
            PlayerHit(paddle);
            AudioManager.instance.PlaySound("BallHit");
            // Devolvemos false
            return false;
            
        }

        // Si colisiona con el l�mite inferior
        if (hit.transform.CompareTag("BottomLimit")) {
            // Notificamos al GameManager que se ha perdido una vida
            GameManager.instance.LifeLost();
            // Resetamos la bola
            isMoving = false;
            //Devolvemos false
            return false;
        }

        // Si la pelota colisiona con un bloque. (Tambien podemos obtener el block fuera de la condici�n)
        if (hit.transform.CompareTag("Block") && hit.transform.TryGetComponent(out BlockController block)) {
            // Notificamos al bloque para su destrucci�n
            block.BlockHit();
        }

        AudioManager.instance.PlaySound("BallHit");

        return true;
    }

    /// <summary>
    /// Gestiona la colision con el jugador (la pala)
    /// </summary>
    private void PlayerHit(PaddleController paddle) {
        // Obtenemos las dos posiciones
        Vector2 ballPosition = transform.position;
        Vector2 paddlePosition = paddle.GetOrigin();
        // Calculamos la direcci�n
        Vector2 direction = (ballPosition - paddlePosition).normalized;
        // Reseteamos la velocidad de la bola
        _velocity = direction * moveSpeed;
    }
}
