using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuSpawnController : MonoBehaviour
{
    // Array de GameObjects con los PU
    public GameObject[] powerUps;
    // Instancia del Singletone
    public static PuSpawnController instance;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else {
            Destroy(this);
        }
    }

    /// <summary>
    /// Devuelve un �ndice aleatorio dentro del rango del array de powerups
    /// </summary>
    /// <returns></returns>
    public int GetRandomPuIndex() {
        return Random.Range(0, powerUps.Length);
    }

    /// <summary>
    /// Spawnea el powerup almadenado en index en la posici�n del parent.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="parent"></param>
    public void SpawnPU(int index, Transform parent) {
        Instantiate(powerUps[index], parent.position, Quaternion.identity);
    }
}
