using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    // Tipo de Power Up
    [Header("Power Up Type")]
    public bool isHeart = false;
    public bool isGun = false;
    public bool isGlue = false;
    public bool isSlowness = false;
    public bool isDoubleScore = false;

    [Header("Movement")]
    public float moveSpeed = 5f;
    public Vector3 direction = Vector3.down;

    [Header("Timer Values")]
    // Timer
    public float startTime = 5;
    public float currentTime = 0;
    public bool activateTimer = false;

    private void OnValidate() {
        if (isHeart) {
            isGun = false;
            isGlue = false;
        }

        if (isGun) {
            isHeart = false;
            isGlue = false;
        }

        if (isGlue) {
            isGun = false;
            isHeart = false;
        }
    }

    private void Start() {
        currentTime = startTime;
    }

    private void Update() {
        if (activateTimer) {
            Timer();
        }

        transform.position += moveSpeed * direction * Time.deltaTime;
    }

    /// <summary>
    /// Gestiona la colisi�n del objeto. Dependiendo del tipo del PU que es
    /// activa un powerUp
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other) {

        if (other.CompareTag("Player")) {
            if (isHeart) {
                GameManager.instance.AddLifePU();
                GameManager.instance.hud.UpdateLivesText(GameManager.instance.currentLives.ToString());
                AudioManager.instance.PlaySound("ExtraLife");
                Destroy(gameObject);
            }else if (isGun) {
                GameManager.instance.EnableLaserPowerUp();
                AudioManager.instance.PlaySound("Laser");
                Destroy(gameObject);
            }else if (isSlowness) {
                // Reducimos la velocidad
                GameManager.instance.ball.moveSpeed = 5f;
                AudioManager.instance.PlaySound("Slowness");
                moveSpeed = 1000f;
                // Activamos timer y dentro del timer destruimos el objeto  
                activateTimer = true;
            }else if (isDoubleScore) {
                GameManager.instance.scoreToUpdate = 20;
                AudioManager.instance.PlaySound("DoubleScore");
                moveSpeed = 1000f;
                activateTimer = true;
            }
        }
    }

    /// <summary>
    /// Timer para los power ups de Slowness y Double Score
    /// </summary>
    private void Timer() {
        currentTime -= 1 * Time.deltaTime;
        if(currentTime <= 0) {
            activateTimer = false;
            if (isSlowness) {
                GameManager.instance.ball.moveSpeed = 10f;
                Destroy(gameObject);
            }
            if (isDoubleScore) {
                GameManager.instance.scoreToUpdate = 10;
                Destroy(gameObject);
            }
        }
    }
}
