using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectileController : MonoBehaviour
{
    public Vector3 direction = Vector3.up;
    public float moveSpeed = 10f;

    public float rayOffset = 0.1f;
    public float rayLenght = 0.1f;

    private Vector2 _velocity;
    private bool colision;

    // Update is called once per frame
    void Update()
    {
        Move();
        colision = CheckCollision(direction);
    }

    private void OnDrawGizmos() {
        // Indicamos el color del gizmo
        Gizmos.color = Color.red;

        // Los dibujamos
        DrawRayGizmo(transform.up);
    }

    /// <summary>
    /// Dibuja un gizmo en la direcci�n indicada por �
    /// </summary>
    /// <param name="direction"></param>
    private void DrawRayGizmo(Vector3 direction) {
        Vector3 from = transform.position + direction * rayOffset;
        Vector3 to = from + direction * rayLenght;
        Gizmos.DrawLine(from, to);
    }

    /// <summary>
    /// Mueve el proyectil
    /// </summary>
    private void Move() {
        _velocity = direction * moveSpeed;
        transform.position += (Vector3)_velocity * Time.deltaTime;
    }

    /// <summary>
    /// Gestiona la colisi�n del proyectil con el bloque
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    private bool CheckCollision(Vector3 direction) {
        Vector3 from = transform.position + direction * rayOffset;
        RaycastHit2D hit = Physics2D.Raycast(from, direction, rayLenght);

        if(hit.transform == null || hit.transform == transform || hit.transform.CompareTag("Player")) {
            return false;
        }

        if (hit.transform.CompareTag("Block") && hit.transform.TryGetComponent(out BlockController block)) {
            // Notificamos al bloque para su destrucci�n
            block.BlockHit();
            Destroy(gameObject);
        }

        if (hit.transform.CompareTag("UpperLimit")) {
            // TODO cambiar a destroy
            Destroy(gameObject);
        }

        return true;
    }
}
