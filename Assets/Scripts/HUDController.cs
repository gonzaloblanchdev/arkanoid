using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    // Referencias a los objetos de tipo text
    public GameObject score;
    public GameObject lives;

    // Variables para almacenar los componentes de tipo text
    public Text scoreText;
    public Text livesText;

    private void Awake() {
        scoreText = score.GetComponent<Text>();
        livesText = lives.GetComponent<Text>();
    }

    /// <summary>
    /// Actualiza el texto del Score 
    /// </summary>
    /// <param name="textToUpdate"></param>
    public void UpdateScoreText(string textToUpdate) {
        scoreText.text = textToUpdate;
    }

    /// <summary>
    /// Actualiza el texto de las vidas
    /// </summary>
    /// <param name="textToUpdate"></param>
    public void UpdateLivesText(string textToUpdate) {
        livesText.text = textToUpdate;
    }
}
