using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    // Color del srpite del bloque
    [Header("Blocks Colors")]
    public Color color = Color.white;
    public Color MediumBlockColor = Color.yellow;
    public Color hardBlockColor = Color.red;

    [Header("Block Life")]
    public int blockLife;

    public bool isMediumBlock = false;
    public bool isHardBlock   = false;

    [SerializeField]
    private bool hasPowerUp = false;

    [SerializeField]
    private SpriteRenderer sprite;

    private void OnValidate() {
        InitializeSprite();
        if (isMediumBlock) {
            blockLife = 2;
            isHardBlock = false;
        } else if (isHardBlock) {
            blockLife = 3;
            isMediumBlock = false;
        } else {
            blockLife = 1;
        }
    }

    void Start(){
        int randomNum = Random.Range(0, 20);
        if(randomNum == 1) {
            hasPowerUp = true;
        }
    }

    private void InitializeSprite() {
        /*
        // Si el sprite no est� referenciado
        if(sprite == null) {
            // Obtenemos el componente
            sprite = GetComponentInChildren<SpriteRenderer>();
        }
        // Le asignamos el color
        sprite.color = color;
        */
        // Si el sprite no est� referenciado, obtenemos el componente SpriteRenderer
        if(sprite == null) {
            sprite = GetComponentInChildren<SpriteRenderer>();
        }

        if (isHardBlock) {
            sprite.color = hardBlockColor;
        }else if (isMediumBlock) {
            sprite.color = MediumBlockColor;
        } else {
            sprite.color = color;
        }
    }

    /// <summary>
    /// M�todo que gestiona la vida de los bloques cuando son dados
    /// </summary>
    public void BlockHit() {
        blockLife -= 1;
        if(blockLife <= 0) {
            RemoveBlock();
            GameManager.instance.UpdateScore();
        }
        ChangeColorOnHit();
    }

    /// <summary>
    /// Cambia el color del bloque cuando es golpeado.
    /// </summary>
    public void ChangeColorOnHit() {
        if (blockLife == 3) {
            sprite.color = hardBlockColor;
        } else if (blockLife == 2) {
            sprite.color = MediumBlockColor;
        } else {
            sprite.color = color;
        }
    }

    /// <summary>
    /// M�todo que gestiona la eliminaci�n del bloque
    /// </summary>
    public void RemoveBlock() {
        // Notificamos al GameManager de que el bloque ha sido destruido
        GameManager.instance.BlockHasBeenRemoved();
        // Generamos power up si tiene
        if (hasPowerUp) {
            PuSpawnController.instance.SpawnPU(PuSpawnController.instance.GetRandomPuIndex(), transform);
            Debug.Log("PU Spawned");
        }
        // Destruimos el bloque
        gameObject.SetActive(false);
        AudioManager.instance.PlaySound("BlockExplosion");
    }
}
