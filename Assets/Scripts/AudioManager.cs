using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    // Array de tipo Sound definido aparte
    public Sound[] sounds;
    // Instancia del singletone
    public static AudioManager instance;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
            return;
        }
        // Para que no se reproduzca doble y siga reproduciéndose en cambio de escena
        DontDestroyOnLoad(gameObject);
        // Inicializamos el Audio Source de cada sonido
        foreach(Sound s in sounds) {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.loop = s.loop;
        }
    }

    private void Start() {
        // Al iniciar el juego reproducimos la musica del juego
        PlaySound("MainTheme");
    }

    /// <summary>
    /// Función que reproduce el clip con el nombre pasado por el paramentro name
    /// </summary>
    /// <param name="soundName"></param>
    public void PlaySound(string soundName) {
        // Buscamos el sonido con el nombre. Es equivalente a usar un foreach.
        Sound s = Array.Find(sounds, sound => sound.name == soundName);
        s.source.Play();
    }

    /// <summary>
    /// Para la reproducción del sonido con nombre "soundName"
    /// </summary>
    /// <param name="soundName"></param>
    public void StopSound(string soundName) {
        Sound s = Array.Find(sounds, sound => sound.name == soundName);
        s.source.Stop();
    }

    /// <summary>
    /// Mutea la música del juego
    /// </summary>
    /// <param name="newState"></param>
    public void MuteMusic(bool newState) {
        Sound s = Array.Find(sounds, sound => sound.tag == "Music");
        s.source.mute = !newState;
    }

    /// <summary>
    /// Mutea todos los efectos especiales del juego
    /// </summary>
    /// <param name="newState"></param>
    public void MuteFX(bool newState) {
        // Buscamos en el array de sonidos los sonidos con el tag FX
        foreach(Sound s in sounds) {
            if(s.tag == "FX") {
                s.source.mute = !newState;
            }
        }
    }
}
