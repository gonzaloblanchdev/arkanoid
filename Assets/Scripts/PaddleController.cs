using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public float moveSpeed = 10f;
    public Vector2 originOffset;
    [Header("Intervalo de posici�n de X")]
    public float minXPosition = -7.68f;
    public float maxXPosition = 7.68f;
    public bool canMove = true;

    void Start()
    {
        
    }

    void Update(){
        if (canMove) {
            Movement();
        }       
    }

    private void OnDrawGizmos() {
        // Gizmo offset del centro de la esfera
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(GetOrigin(), 0.05f);
        // Gizmos de los l�mites de posici�n en X
        Gizmos.color = Color.yellow;
        Vector2 left  = new Vector2(minXPosition, transform.position.y);
        Vector2 right = new Vector2(maxXPosition, transform.position.y);
        Gizmos.DrawLine(left, right);
    }

    /// <summary>
    /// Mueve la pala dentro del intervalo del eje X
    /// </summary>
    private void Movement() {
        float xInput = Input.GetAxisRaw("Horizontal");
        Vector3 input = new Vector3(xInput, 0f);
        Vector3 step = input * moveSpeed * Time.deltaTime;
        // Nos guardamos el valor de la siguiente posici�n
        Vector3 nextPosition = transform.position + step;
        // Clampeamos su posici�n en X
        nextPosition.x = Mathf.Clamp(nextPosition.x, minXPosition, maxXPosition);
        // Aplicamos el movimiento
        transform.position = nextPosition;

    }

    /// <summary>
    /// Devuelve la posici�n de la pala con el offset aplicado
    /// </summary>
    /// <returns></returns>
    public Vector3 GetOrigin() {
        return transform.position + (Vector3) originOffset;
    }

}
